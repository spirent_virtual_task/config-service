# config-service

Stores the desired configuration devices. When adding a new device to the registry, it requests from configuration previously saved there the configuration and applies it to the device.